class Video{
  int videoId;
  String videoName;
  String videoUrl;
  String videoDescription;
  String videoCreator;
  String videoCategory;
  String videoUpload;

  Video({
    this.videoId,
    this.videoName,
    this.videoUrl,
    this.videoDescription,
    this.videoCreator,
    this.videoCategory,
    this.videoUpload
 });

  factory Video.fromJson(Map<String,dynamic> listVideo) {
      return Video(
      videoId : listVideo['id'],
      videoName : listVideo['nama_video'],
      videoUrl : listVideo['alamat_video'],
      videoDescription : listVideo['deskripsi_video'],
      videoCreator : listVideo['nama_kreator'],
      videoCategory : listVideo['kategori'],
      videoUpload : listVideo['tanggal_unggah']
    );}
}
//class Recomendation(String ss) {
//  return "Rekomendasi"
//}