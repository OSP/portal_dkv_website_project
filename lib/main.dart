import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:portal_dekave/master_api.dart';
import 'package:portal_dekave/video.dart';
import 'package:portal_dekave/video_player.dart';
import 'package:video_player/video_player.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var videos = new List<Video>();
  int _index = 0;

  _getVideos() {
    MasterApi.videoList().then((response) {
      setState(() {
        Iterable list = json.decode(response.body);
        videos = list.map((model) => Video.fromJson(model)).toList();
      });
    });
  }

  initState() {
    super.initState();
    _getVideos();
  }

  dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget child = Container();

    switch(_index) {
      case 0:
        child = 
        new Center(
          child: 
            ListView.builder(
              itemCount: videos.length,
              itemBuilder: (context, index) {
                
                return Column(
                  children: <Widget>[
                    GestureDetector(
                      onTap: (){
                        child = 
                        new Center(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Expanded(child: PortalVideoPlayer(
                                videoPlayerController: VideoPlayerController.network(
                                  videos[index].videoUrl,
                                ),
                              ), flex: 10,),
                              Expanded(
                                child: Column(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(top: 5.5, bottom: 4.0, left: 5.0),
                                      child: Text(videos[index].videoName, style: TextStyle(fontSize: 18.0),),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 5.0),
                                      child: Text(videos[index].videoDescription.substring(0, 80) + "....", style: TextStyle(color: Colors.black54),),
                                    )
                                  ],
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                ),
                                flex: 9,
                              ),
                              Expanded(child: Icon(Icons.more_vert), flex: 1,),
                            ],
                          ),
                        );
                      },
                    ),
                    Padding(
                      
                      padding: const EdgeInsets.only(left: 15.0, right: 15.0, top: 15.0, bottom: 5.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Expanded(child: PortalVideoPlayer(
                            videoPlayerController: VideoPlayerController.network(
                              videos[index].videoUrl,
                            ),
                          ), flex: 10,),
                          Expanded(
                            child: Column(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(top: 5.5, bottom: 4.0, left: 5.0),
                                  child: Text(videos[index].videoName, style: TextStyle(fontSize: 18.0),),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 5.0),
                                  child: Text(videos[index].videoDescription.substring(0, 80) + "....", style: TextStyle(color: Colors.black54),),
                                )
                              ],
                              crossAxisAlignment: CrossAxisAlignment.start,
                            ),
                            flex: 9,
                          ),
                          Expanded(child: Icon(Icons.more_vert), flex: 1,),
                        ],
                      ),
                    )
                  ],
                  
                );
              },
            ),
          );
        break;

      case 1:
        child = Text("Ngapain upload? Upload itu adalah sebuah mitos :p");
        break;

      case 2:
        child = Text("Tak ada notif untukmu :p");
        break;

      case 3:
        child = Text("Emang kamu punya profil :p");
        break;
    }

    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.blue,
        title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                  Image.asset(
                 'assets/images/pordev_putih.png',
                  fit: BoxFit.contain,
                  height: 60,
              ),
              Container(
                  padding: const EdgeInsets.all(8.0))
            ],

          ),
      
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12.0),
            child: Icon(Icons.search, color: Colors.black54,),
          ),
        ],
      ),
      body: SizedBox.expand(child: child),
      bottomNavigationBar: _bottomTab()
      
    );
  }

  Widget _bottomTab() {
    return BottomNavigationBar(
      currentIndex: _index,
      
      onTap: (int index) => setState(() => _index = index),
      items: [
        BottomNavigationBarItem(icon: Icon(Icons.video_library, color: Colors.black54,), title: Text("Jelajah", style: TextStyle(color: Colors.black54),),),
        BottomNavigationBarItem(icon: Icon(Icons.add_circle_outline, color: Colors.black54,), title: Text("Unggah", style: TextStyle(color: Colors.black54),),),
        BottomNavigationBarItem(icon: Icon(Icons.notifications, color: Colors.black54,), title: Text("Notifikasi", style: TextStyle(color: Colors.black54),),),
        BottomNavigationBarItem(icon: Icon(Icons.person_outline, color: Colors.black54,), title: Text("Profil", style: TextStyle(color: Colors.black54),),),
      ], type: BottomNavigationBarType.fixed,);
  }
}