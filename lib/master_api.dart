import 'dart:async';
import 'package:http/http.dart' as http;

const baseUrl = "http://10.0.3.2:3000/data_api/mvp";

class MasterApi {
  static Future videoList() {
    var url = baseUrl + "/videos";
    return http.get(url);
  }
  static Future videoPick(String idVideonya) {
    var url = baseUrl + "/videos/" + idVideonya;
    return http.get(url);
  }
}